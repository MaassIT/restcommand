# Simple Rest Command Executor

This tool allows you to create a REST-Endpoint for execute every command on an Unix-Based Endpoint (Linux, MacOS, Unix).

It is written in C#.

To use it install the .net core SDK and compile it with "dotnet publish". Then you can run the executable file (named just 'Server'). To configure the IP and port which the service should listen, start the server with './Server --urls "http://localhost:8080" '

ToDo: How to start it in Background

## How to use it

If the Server is running, you can execute any executable on the machine by just make an POST-Http-Request on an url the Server is listen on. For example if you want to ping google.com you can make a POST-Request to "http://localhost:5000/sbin/ping" and pass a header names "args" with the value "-c 3 google.com".
