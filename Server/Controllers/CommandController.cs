﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Server.Controllers
{
    [ApiController]
    public class CommandController : ControllerBase
    {
        private readonly ILogger<CommandController> _logger;

        public CommandController(ILogger<CommandController> logger)
        {
            _logger = logger;
        }

        [HttpPost("{*url}")]
        public async Task<IActionResult> RunCommand(CancellationToken cancellationToken)
        {
            try
            {
                var escapedArgs = Request.Headers["args"].FirstOrDefault()?.Replace("\"", "\\\"");
                
                _logger.LogInformation("Starting process \"{Command} {EscapedArgs}\"", 
                    Request.Path, escapedArgs);
                
                var process = new Process()
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = Request.Path,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        UseShellExecute = false,
                        CreateNoWindow = true, 
                    }
                };

                if (escapedArgs != null)
                    process.StartInfo.Arguments = escapedArgs;

                process.Start();
                await process.WaitForExitAsync(cancellationToken);
                string result = await process.StandardOutput.ReadToEndAsync();
                string error = await process.StandardError.ReadToEndAsync();
                
                if (string.IsNullOrWhiteSpace(error)) return Ok(result);
                
                _logger.LogError("Error while execution: {Message}", error);
                return Problem(error);
            }
            catch (TaskCanceledException)
            {
                _logger.LogInformation("Command {Path} canceled", Request.Path);
                return NoContent();
            }
            catch (Exception e)
            {
                _logger.LogError("Error while execution: {Message}", e.Message);
                return Problem(e.Message);
            }
        }
    }
}
